//
//  FlickrPhotoTableCell.swift
//  Searchy
//
//  Created by Alena Starikova on 7/1/19.
//  Copyright © 2019 starikova. All rights reserved.
//

import UIKit

class FlickrPhotoTableCell: UITableViewCell {
    
    let titleLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 21.0, weight: .medium)
        label.textColor = UIColor.black
        label.textAlignment = .left
        label.numberOfLines = 0
        label.adjustsFontSizeToFitWidth = true
        return label
    }()
    
    let photoImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.backgroundColor = UIColor.groupTableViewBackground
        imageView.layer.cornerRadius = 6.0
        imageView.clipsToBounds = true
        return imageView
    }()
    
    var flickrPhoto: FlickrPhoto? {
        didSet {
            titleLabel.text = flickrPhoto?.title
        }
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    fileprivate func setupUI() {
        contentView.addSubview(titleLabel)
        contentView.addSubview(photoImageView)
        
        photoImageView.addWidthConstraint(toView: nil, relation: .equal, constant: 60.0)
        photoImageView.addHeightConstraint(toView: nil, relation: .equal, constant: 60.0)
        photoImageView.addLeadingConstraint(toView: contentView, attribute: .leading, relation: .equal, constant: 16.0)
        photoImageView.addTopConstraint(toView: contentView, attribute: .top, relation: .equal, constant: 11.0)
        photoImageView.addBottomConstraint(toView: contentView, attribute: .bottom, relation: .greaterThanOrEqual, constant: -8.0)
        titleLabel.addLeftConstraint(toView: photoImageView, attribute: .right, relation: .equal, constant: 16.0)
        titleLabel.addTrailingConstraint(toView: contentView, attribute: .trailing, relation: .equal, constant: 16.0)
        titleLabel.addTopConstraint(toView: photoImageView, attribute: .top, relation: .equal, constant: 0.0)
        titleLabel.addBottomConstraint(toView: contentView, attribute: .bottom, relation: .equal, constant: -8.0)
    }

}
