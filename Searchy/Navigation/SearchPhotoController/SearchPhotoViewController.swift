//
//  SearchPhotoViewController.swift
//  Searchy
//
//  Created by Alena Starikova on 7/1/19.
//  Copyright © 2019 starikova. All rights reserved.
//

import UIKit

class SearchPhotoViewController: UIViewController {
    
    let tableView: UITableView = {
        let table = UITableView(frame: .zero, style: .plain)
        return table
    }()
    
    let activityIndicator: UIActivityIndicatorView = {
        let indicator = UIActivityIndicatorView(style: .whiteLarge)
        indicator.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        indicator.hidesWhenStopped = true
        return indicator
    }()

    var photos: [FlickrPhoto] = []
    
    var presenter: SearchPhotoPresenter!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter = SearchPhotoPresenter(view: self)
        addKeyboardObserver()
        setupTableView()
        setupActivityIndicator()
        requestData()
        setupSearchController()
    }
    
    fileprivate func requestData() {
        activityIndicator.startAnimating()
        presenter.getHistory()
    }
    
    fileprivate func setupTableView() {
        view.addSubview(tableView)
        tableView.fillSuperView()
        
        tableView.dataSource = self
        tableView.delegate = self
        tableView.keyboardDismissMode = .interactive
        tableView.register(FlickrPhotoTableCell.self, forCellReuseIdentifier: "FlickrPhotoTableCell")
        tableView.reloadData()
    }
    
    fileprivate func setupActivityIndicator() {
        view.addSubview(activityIndicator)
        activityIndicator.fillSuperView()
    }
    
    fileprivate func setupSearchController() {
        let search = UISearchController(searchResultsController: nil)
        search.obscuresBackgroundDuringPresentation = false
        search.searchBar.placeholder = "Type something here to search"
        search.searchBar.delegate = self
        definesPresentationContext = true
        navigationItem.searchController = search
        navigationItem.hidesSearchBarWhenScrolling = false
        navigationItem.title = "Flickr Search"
    }
    
    func addKeyboardObserver() {
        NotificationCenter.default.addObserver(self, selector: #selector(systemKeyboardWillShow), name: UIResponder.keyboardWillShowNotification,object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(systemKeyboardWillHide), name: UIResponder.keyboardWillHideNotification , object: nil)
    }
    
    @objc func systemKeyboardWillShow(_ notification: Notification) {
        if let keyboardFrame: NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardRectangle = keyboardFrame.cgRectValue
            let keyboardHeight = keyboardRectangle.height
            tableView.contentInset = UIEdgeInsets(top: tableView.contentInset.top, left: tableView.contentInset.left, bottom: keyboardHeight, right: tableView.contentInset.right)
        }
    }
    
    @objc func systemKeyboardWillHide(_ notification: Notification) {
        tableView.contentInset = UIEdgeInsets(top: tableView.contentInset.top, left: tableView.contentInset.left, bottom: 0.0, right: tableView.contentInset.right)
    }
    
    func downloadImage(from url: String, cell: FlickrPhotoTableCell, at indexPath: IndexPath) {
        ImageDownloadManager.shared.downloadImage(url, indexPath: indexPath) { (cgImage, url, passedIndexPath, error) in
            let newURL = self.photos[indexPath.row].photoUrl
            if let indexPathNew = passedIndexPath, indexPathNew == indexPath, newURL == url, let newImage = cgImage {
                DispatchQueue.main.async {
                    cell.photoImageView.image = UIImage(cgImage: newImage)
                }
            } else {
                DispatchQueue.main.async {
                    cell.photoImageView.image = nil
                }
            }
        }
    }
}

extension SearchPhotoViewController: SearchPhotoView {
    
    func didGetPhoto(_ photo: FlickrPhoto) {
        self.photos.insert(photo, at: 0)
        
        activityIndicator.stopAnimating()
        
        self.tableView.beginUpdates()
        self.tableView.insertRows(at: [IndexPath(row: 0, section: 0)], with: .top)
        self.tableView.endUpdates()
        
    }
    
    func didGetHistory(_ photos: [FlickrPhoto]) {
        self.photos = photos
        self.tableView.reloadData()
        activityIndicator.stopAnimating()
    }
    
    func didRecieveError(_ message: String) {
        activityIndicator.stopAnimating()

        let alertController = UIAlertController(title: "Error", message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(okAction)
        navigationController?.present(alertController, animated: true, completion: nil)
    }
}

extension SearchPhotoViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return photos.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FlickrPhotoTableCell", for: indexPath) as! FlickrPhotoTableCell
        cell.flickrPhoto = photos[indexPath.row]
        cell.photoImageView.image = nil
        let url = photos[indexPath.row].photoUrl.absoluteString
        downloadImage(from: url, cell: cell, at: indexPath)
        return cell
    }
    
}

extension SearchPhotoViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80.0
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row < photos.count {
            let url = photos[indexPath.row].photoUrl.absoluteString
            downloadImage(from: url, cell: (cell as! FlickrPhotoTableCell), at: indexPath)
        }
    }
    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row < photos.count {
            let url = photos[indexPath.row].photoUrl.absoluteString
            ImageDownloadManager.shared.slowDownImageDownloadTaskfor(url)
        }
    }
}

extension SearchPhotoViewController: UISearchBarDelegate {
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        if let text = searchBar.text {
            activityIndicator.startAnimating()
            presenter.getPhoto(text)
        }
    }
}
