//
//  SearchPhotoPresenter.swift
//  Searchy
//
//  Created by Alena Starikova on 7/2/19.
//  Copyright © 2019 starikova. All rights reserved.
//

import UIKit

protocol SearchPhotoView: class {
    func didGetPhoto(_ photo: FlickrPhoto)
    func didGetHistory(_ photos: [FlickrPhoto])
    
    func didRecieveError(_ message: String)
}

protocol SearchPhotoViewPresenter {
    init(view: SearchPhotoView)
    
    func getPhoto(_ text: String)
    func getHistory()
}

class SearchPhotoPresenter: SearchPhotoViewPresenter {
    
    weak var view: SearchPhotoView?
    
    required init(view: SearchPhotoView) {
        self.view = view
    }
    
    func getPhoto(_ text: String) {
        FlickrProvider().getPhoto(text: text) { (results, error) in
            if let result = results?.first {
                self.view?.didGetPhoto(result)
            } else if let error = error {
                self.view?.didRecieveError(error)
            } else {
                self.view?.didRecieveError(NetworkResponse.noData.rawValue)
            }
        }
    }
    
    func getHistory() {
        FlickrProvider().getHistory { (photos, error) in
            if let photos = photos {
                self.view?.didGetHistory(photos)
            } else if let error = error {
                self.view?.didRecieveError(error)
            } else {
                self.view?.didRecieveError(NetworkResponse.noData.rawValue)
            }
        }
    }
}
