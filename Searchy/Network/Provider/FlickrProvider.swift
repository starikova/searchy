//
//  FlcikrProvider.swift
//  Searchy
//
//  Created by Alena Starikova on 7/1/19.
//  Copyright © 2019 starikova. All rights reserved.
//

import Foundation
import RealmSwift

struct FlickrProvider {
    static let environment : NetworkEnvironment = .production
    static let apiKey = "d1adcd5bebff29a8821433b36aaf4889"
    static let apiSecret = "445d765afa6bd0e4"
    
    let router = Router<FlickrApi>()

    func getPhoto(text: String, completion: @escaping (_ photo: [FlickrPhoto]?,_ error: String?)->()){
        router.request(.search(text: text)) { data, response, error in

            if error != nil {
                completion(nil, "Please check your network connection.")
            }

            if let response = response as? HTTPURLResponse {
                let result = self.handleNetworkResponse(response)
                switch result {
                case .success:
                    guard let responseData = data else {
                        completion(nil, NetworkResponse.noData.rawValue)
                        return
                    }
                    do {
                        let apiResponse = try JSONDecoder().decode(FlickrPhotosApiResponse.self, from: responseData)
                        if let photo = apiResponse.photos.photo.first {
                            let newObject = self.savePhoto(photo, requestText: text)
                            completion([FlickrPhoto(from: newObject)],nil)
                        } else {
                            completion(nil, NetworkResponse.noData.rawValue)
                        }
                    } catch {
                        completion(nil, NetworkResponse.unableToDecode.rawValue)
                    }
                case .failure(let networkFailureError):
                    completion(nil, networkFailureError)
                }
            }
        }
    }
    
    func getHistory(completion: @escaping (_ photo: [FlickrPhoto]?,_ error: String?)->()) {
        let realm = try! Realm()
        let results = realm.objects(FlickrPhotoObject.self).reversed().map { FlickrPhoto(from: $0) }
        completion(Array(results), nil)
    }
    
    fileprivate func savePhoto(_ photo: FlickrPhoto, requestText: String) -> FlickrPhotoObject {
        let realm = try! Realm()
        
        let photoObject = FlickrPhotoObject()
        
        photoObject.photoId = photo.photoId ?? ""
        photoObject.farm = photo.farm ?? 0
        photoObject.secret = photo.secret ?? ""
        photoObject.server = photo.server ?? ""
        photoObject.requestTitle = requestText
        
        do {
            try realm.write {
                realm.add(photoObject)
            }
        } catch {
            
        }
        return photoObject
    }
    
    fileprivate func handleNetworkResponse(_ response: HTTPURLResponse) -> Result<String>{
        switch response.statusCode {
        case 200...299: return .success
        case 401...500: return .failure(NetworkResponse.authenticationError.rawValue)
        case 501...599: return .failure(NetworkResponse.badRequest.rawValue)
        case 600: return .failure(NetworkResponse.outdated.rawValue)
        default: return .failure(NetworkResponse.failed.rawValue)
        }
    }
}

