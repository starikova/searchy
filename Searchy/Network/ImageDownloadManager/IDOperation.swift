//
//  IDOperation.swift
//  Rooky
//
//  Created by Alena Starikova on 12/29/18.
//  Copyright © 2018 Developer. All rights reserved.
//
// Image Download Operation
import Foundation
import ImageIO

class IDOperation: Operation {
    
    var downloadHandler: ImageDownloadHandler?
    
    var imageURL: URL!
    
    private var indexPath: IndexPath?
    
    override var isAsynchronous: Bool {
        get {
            return true
        }
    }
    
    private var _executing = false {
        willSet {
            willChangeValue(forKey: "isExecuting")
        }
        didSet {
            didChangeValue(forKey: "isExecuting")
        }
    }
    
    override var isExecuting: Bool {
        return _executing
    }
    
    private var _finished = false {
        willSet {
            willChangeValue(forKey: "isFinished")
        }
        didSet {
            didChangeValue(forKey: "isFinished")
        }
    }
    
    override var isFinished: Bool {
        return _finished
    }
    
    func executing(_ executing: Bool) {
        _executing = executing
    }
    
    func finish(_ finished: Bool) {
        _finished = finished
    }
    
    required init(url: URL, indexPath: IndexPath?) {
        self.imageURL = url
        self.indexPath = indexPath
    }
    
    override func main() {
        
        guard isCancelled == false else {
            finish(true)
            return
        }
        
        self.executing(true)
        self.downloadImageFromUrl()
    }
    func downloadImageFromUrl() {
        let newSession = URLSession.shared
        let downloadTask = newSession.downloadTask(with: self.imageURL) { (location, response, error) in
            if let locationURL = location, let data = try? Data(contentsOf: locationURL) {
                let queue = DispatchQueue.global(qos: .utility)
                queue.async {
                    if let imageSource = CGImageSourceCreateWithData(data as CFData, [kCGImageSourceShouldCacheImmediately: true] as CFDictionary) {
                        let options: [NSString: Any] = [kCGImageSourceThumbnailMaxPixelSize: 150,
                                                        kCGImageSourceCreateThumbnailFromImageAlways: true]
                        if let scaledImage = CGImageSourceCreateThumbnailAtIndex(imageSource, 0, options as CFDictionary) {
                            self.downloadHandler?(scaledImage, self.imageURL, self.indexPath, error)
                        }
                    }
                }
            }
            self.finish(true)
            self.executing(false)
        }
        downloadTask.resume()
    }
}
