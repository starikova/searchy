//
//  ImageDownloadManager.swift
//  Rooky
//
//  Created by Alena Starikova on 12/29/18.
//  Copyright © 2018 Developer. All rights reserved.
//

import Foundation
import UIKit
import ImageIO

typealias ImageDownloadHandler = (_ image: CGImage?, _ url: URL, _ indexPath: IndexPath?, _ error: Error?) -> Swift.Void

final class ImageDownloadManager {
    
    private var completionHandler: ImageDownloadHandler?
    
    lazy var imageDownloadQueue: OperationQueue = {
        var queue = OperationQueue()
        queue.name = "com.starikova.imageDownloadQueue"
        queue.qualityOfService = .userInteractive
        return queue
    }()
        
    static let shared = ImageDownloadManager()
    
    private init() {}
    
    func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return paths[0]
    }
    
    func downloadImage(_ urlString: String, indexPath: IndexPath?, handler: @escaping ImageDownloadHandler) {
        self.completionHandler = handler
        
        guard let url = URL(string: urlString) else { return }
        let fileManager = FileManager.default
        let diskPath = self.getDocumentsDirectory().appendingPathComponent(url.lastPathComponent).path
        
        if fileManager.fileExists(atPath: diskPath) {
            let cachedImage = UIImage(contentsOfFile: diskPath)?.cgImage
            self.completionHandler?(cachedImage, url, indexPath, nil)
        } else {
            if let operations = (imageDownloadQueue.operations as? [IDOperation])?.filter({$0.imageURL.absoluteString == url.absoluteString && $0.isFinished == false && $0.isExecuting == true}), let operation = operations.first {
                operation.queuePriority = .high
            } else {
                
                let operation = IDOperation(url: url, indexPath: indexPath)
                if indexPath == nil {
                    operation.queuePriority = .veryHigh
                }
                operation.downloadHandler = { (image, url, indexPath, error) in
                    if let newImage = image {
                        let uiImage = UIImage(cgImage: newImage)
                        let data = uiImage.pngData()!
                        let filename = self.getDocumentsDirectory().appendingPathComponent(url.lastPathComponent)
                        do {
                            try data.write(to: filename)

                        } catch {
                            print(error)
                        }
                    }
                    self.completionHandler?(image, url, indexPath, error)
                }
                imageDownloadQueue.addOperation(operation)
            }
        }
    }
    
    func slowDownImageDownloadTaskfor(_ urlString: String) {
        guard let url = URL(string: urlString) else { return }
        if let operations = (imageDownloadQueue.operations as? [IDOperation])?.filter({$0.imageURL.absoluteString == url.absoluteString && $0.isFinished == false && $0.isExecuting == true}), let operation = operations.first {
            operation.queuePriority = .low
        }
    }
}
