//
//  FlickrEndPoint.swift
//  Searchy
//
//  Created by Alena Starikova on 7/1/19.
//  Copyright © 2019 starikova. All rights reserved.
//
import Foundation

public enum FlickrApi {
    case search(text: String)
}

extension FlickrApi: EndPointType {
    
    var environmentBaseURL : String {
        switch FlickrProvider.environment {
        case .production: return "https://api.flickr.com/services/rest/"
        case .qa: return "https://api.flickr.com/services/rest/"
        case .staging: return "https://api.flickr.com/services/rest/"
        }
    }
    
    var baseURL: URL {
        guard let url = URL(string: environmentBaseURL) else { fatalError("baseURL could not be configured.")}
        return url
    }
    
    var path: String {
        switch self {
        case .search(let _):
            return ""
        }
    }
    
    var httpMethod: HTTPMethod {
        return .get
    }
    
    var task: HTTPTask {
        switch self {
        case .search(let text):
            return .requestParameters(bodyParameters: nil,
                                      bodyEncoding: .urlEncoding,
                                      urlParameters: ["method": "flickr.photos.search",
                                                      "api_key": FlickrProvider.apiKey,
                                                      "tags": text,
                                                      "per_page": "1",
                                                      "format": "json",
                                                      "nojsoncallback": "1"])
        }
    }
    
    var headers: HTTPHeaders? {
        return nil
    }
}


