//
//  AppDelegate.swift
//  Searchy
//
//  Created by Alena Starikova on 7/1/19.
//  Copyright © 2019 starikova. All rights reserved.
//

import UIKit
import RealmSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        window = UIWindow(frame: UIScreen.main.bounds)
        
        let viewController = SearchPhotoViewController()
        let navigationController = UINavigationController(rootViewController: viewController)
        
        window?.rootViewController = navigationController
        window?.makeKeyAndVisible()
        
        setupRealmMigration()
        
        return true
    }

    fileprivate func setupRealmMigration() {
        if let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String {
            
            _ = Realm.Configuration.defaultConfiguration.schemaVersion + 1
            print(version)
        }
        
        if let file = Realm.Configuration.defaultConfiguration.fileURL {
            
            let realmConfig = Realm.Configuration(fileURL: file, inMemoryIdentifier: nil, syncConfiguration: nil, encryptionKey: nil, readOnly: false, schemaVersion: 1, migrationBlock: { (migration, oldSchemaVersion) in
            }, deleteRealmIfMigrationNeeded: false, objectTypes: nil)
            
            Realm.Configuration.defaultConfiguration = realmConfig
        }
    }
    
    
}

