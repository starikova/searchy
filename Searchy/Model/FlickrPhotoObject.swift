//
//  FlickrPhotoObject.swift
//  Searchy
//
//  Created by Alena Starikova on 7/2/19.
//  Copyright © 2019 starikova. All rights reserved.
//

import Foundation
import RealmSwift

class FlickrPhotoObject: Object {
    
    @objc dynamic var requestTitle: String = ""
    @objc dynamic var photoId: String = ""
    @objc dynamic var farm: Int = 0
    @objc dynamic var secret: String = ""
    @objc dynamic var server: String = ""
}
