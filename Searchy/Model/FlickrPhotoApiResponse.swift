//
//  FlickrPhotoApiResponse.swift
//  Searchy
//
//  Created by Alena Starikova on 7/2/19.
//  Copyright © 2019 starikova. All rights reserved.
//

import Foundation

struct FlickrPhotoApiResponse {
    let page: Int
    let pages: Int
    let perPage: Int
    let photo: [FlickrPhoto]
}

extension FlickrPhotoApiResponse: Decodable {
    
    private enum FlickrPhotoApiResponseCodingKeys: String, CodingKey {
        case page
        case pages
        case perPage = "perpage"
        case photo
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: FlickrPhotoApiResponseCodingKeys.self)
        
        page = try container.decode(Int.self, forKey: .page)
        pages = try container.decode(Int.self, forKey: .page)
        perPage = try container.decode(Int.self, forKey: .perPage)
        photo = try container.decode([FlickrPhoto].self, forKey: .photo)
    }
}

struct FlickrPhotosApiResponse {
    let photos: FlickrPhotoApiResponse
}

extension FlickrPhotosApiResponse: Decodable {
    private enum FlickrPhotosApiResponseCodingKeys: String, CodingKey {
        case photos
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: FlickrPhotosApiResponseCodingKeys.self)
        
        photos = try container.decode(FlickrPhotoApiResponse.self, forKey: .photos)
    }
}
