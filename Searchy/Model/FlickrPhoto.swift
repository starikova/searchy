//
//  FlickrPhoto.swift
//  Searchy
//
//  Created by Alena Starikova on 7/1/19.
//  Copyright © 2019 starikova. All rights reserved.
//

import Foundation

struct FlickrPhoto {
    
    var photoId: String?
    var farm: Int?
    var secret: String?
    var server: String?
    var title: String?
    
    var photoUrl: URL {
        let string = "https://farm\(farm!).staticflickr.com/\(server!)/\(photoId!)_\(secret!)_m.jpg"
        return URL(string: string)!
    }
    
}

extension FlickrPhoto: Decodable {
    
    enum FlickrPhotoCodingKeys: String, CodingKey {
        case id
        case farm
        case secret
        case server
        case title
    }
    
    init(from object: FlickrPhotoObject) {
        self.photoId = object.photoId
        self.farm = object.farm
        self.secret = object.secret
        self.server = object.server
        self.title = object.requestTitle
    }
    
    
    init(from decoder: Decoder) throws {
        let photoContainer = try decoder.container(keyedBy: FlickrPhotoCodingKeys.self)
        if let photoID = try photoContainer.decodeIfPresent(String.self, forKey: .id) {
            photoId = photoID
        }
        if let farmID = try photoContainer.decodeIfPresent(Int.self, forKey: .farm) {
            farm = farmID
        }
        if let secretID = try photoContainer.decodeIfPresent(String.self, forKey: .secret) {
            secret = secretID
        }
        if let serverID = try photoContainer.decodeIfPresent(String.self, forKey: .server) {
            server = serverID
        }
        if let titleText = try photoContainer.decodeIfPresent(String.self, forKey: .title) {
            title = titleText
        }
    }
}
